//
// Created by bratjuuc on 15.06.2020.
//

#ifndef TASK3_ABSTRACTLIST_H
#define TASK3_ABSTRACTLIST_H



template <class T>
class AbstractList {
protected:
    class AbsIter{
    public:
        explicit AbsIter(AbstractList<T>& list){}
        virtual AbsIter& insert(T) = 0;
        virtual T extract() = 0;
        virtual AbsIter& next() = 0;
        virtual bool end() const =0;
        virtual T peek() const = 0;
        virtual T operator*() const =0;
        virtual AbsIter& operator++() = 0;
    protected:
    };
public:
    //virtual AbsIter first_occurence(T) = 0;
    ~AbstractList() = default;
    virtual bool isEmpty() const  = 0;
    virtual void makeEmpty() = 0;
    virtual int  getSize() const = 0;
    //virtual AbsIter* begin() = 0;//{return new Iter(this);}
};



#endif //TASK3_ABSTRACTLIST_H
