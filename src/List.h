//
// Created by bratjuuc on 15.06.2020.
//

#ifndef TASK3_LIST_H
#define TASK3_LIST_H
#include "AbstractList.h"



template <class T>
class Linked2List : AbstractList<T>{
protected:
    class Node{
    public:
        Node* next = nullptr;
        Node* prev = nullptr;
        T value;
        Node(){next = prev = this;}
    public:
        //~Node();
        explicit Node(T&);
    };

    class Iterator:AbstractList<T>::AbsIter{
    protected:
        Node* curr = nullptr;

        Linked2List<T>* ptr;
    public:
        friend Linked2List;
        inline bool end() const {return curr == ptr->null;}
        Iterator& insert(T);
        inline T peek() const {if (end()) throw "empty list"; else return curr->value;};
        inline T operator* () const {return peek();}
        T extract();
        inline Iterator& operator++() {curr = curr->next; return *this;}
        inline Iterator& next()       {++*this; return *this;}
        inline Iterator& operator--() {curr = curr->prev; return *this;}
        inline explicit Iterator(Linked2List<T>& list);
    };
private:
    Node* null;
    int size = 0;
public:
    ~Linked2List(){ makeEmpty(); delete null;}
    Linked2List(){ null = new Node();}
    Linked2List(T array[], int size);
    Linked2List& operator=(const Linked2List<T>& copy);
    Linked2List& operator=(Linked2List<T>&& copy) noexcept ;
    Linked2List(Linked2List<T>&& copy) noexcept ;
    Linked2List(Linked2List<T>& copy);
    Linked2List& append(T element);

    inline bool isEmpty() const {return size == 0;}
    inline void makeEmpty();
    inline int  getSize() const {return size;}

    Iterator first_occurence(T val) ;
    Iterator end() const ;

    inline Iterator begin(){return Iterator(*this);}
};



#endif //TASK3_LIST_H
