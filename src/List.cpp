//
// Created by bratjuuc on 15.06.2020.
//

#include "List.h"
#include <iostream>


/*template<class T>
Linked2List<T>::Node::~Node() {
    if (next && prev) {
        next->prev = prev;
        prev->next = next;
    }
    else if (next || prev){
        std::cout << "Error" << __LINE__<< std::endl;
    }
}*/

template<class T>
Linked2List<T>::Node::Node(T &val): value(val) {
    next = prev = this;
}


template<class T>
Linked2List<T> &Linked2List<T>::append(T element) {
    Iterator i = begin();

    i.insert(element);

    return *this;
}

template<class T>
typename Linked2List<T>::Iterator & Linked2List<T>::Iterator::insert(T val) {
    Node* node = new Node(val);

    if (curr != ptr->null)
        curr = curr->prev;
    node->prev = curr;
    node->next = curr->next;
    curr->next = node;
    node->next->prev = node;

    curr = node;

    ptr->size++;
    return *this;
}

template<class T>
T Linked2List<T>::Iterator::extract() {
    if (curr == ptr->null){
        std::cout << __LINE__ ;
        throw "Empty list extraction error";
    } else {
        T result = curr->value;
        Node* nextCurr = curr->next;

        curr->next->prev = curr->prev;
        curr->prev->next = curr->next;
        curr->next = curr->prev = nullptr;

        ptr->size--;
        delete curr;
        curr = nextCurr;// == ptr->null ? newCurr->next : newCurr;

        return result;
    }
}

template<class T>
Linked2List<T>::Iterator::Iterator(Linked2List<T> &list) :AbstractList<T>::AbsIter(list) {
    curr = list.null->next;
    ptr = &list;
}


template <class T>
typename Linked2List<T>::Iterator Linked2List<T>::first_occurence(T val) {
    typename Linked2List<T>::Iterator result = begin();

    while (result.curr != null && result.curr->value != val) {
        ++result;
    }

    if (result.curr != null)
        return result;
    else
        throw "Элемент не найден";
}

template<class T>
Linked2List<T>::Linked2List(T *array, int size) {
    null = new Node();
    Iterator iterator = begin();

    for (int i = 0; i < size;){
        iterator.insert(array[i]);
        ++iterator;
        ++i;
    }
}

template<class T>
Linked2List<T> &Linked2List<T>::operator=(const Linked2List<T> &copy) {
    if (this != &copy){
        makeEmpty();

        Iterator it = begin();
        for (Iterator i = copy.begin(); !i.end();){
            it.insert(*i);
            ++it;
            ++i;
        }
    }
    return *this;
}

template<class T>
Linked2List<T> &Linked2List<T>::operator=(Linked2List<T> &&copy)  noexcept {
    if (this != &copy){
        std::swap(null, copy.null);
        std::swap(size, copy.size);
    }
    return *this;
}

template<class T>
Linked2List<T>::Linked2List(Linked2List<T> &&copy) noexcept {
    if (this != &copy){
        std::swap(null, copy.null);
        std::swap(size, copy.size);
    }
}

template<class T>
Linked2List<T>::Linked2List(Linked2List<T> &copy) {
    null = new Node();

    //Iterator it = begin();
    for (Iterator i = copy.begin(); !i.end();){
        append(*i);
        //++it;
        ++i;
    }
}

template<class T>
void Linked2List<T>::makeEmpty() {
    /*for (Iterator i = begin(); !isEmpty();) {
        i.extract();
    }*/

    Node* tmp, *ptr = null->next;

    while (ptr != null){
        tmp = ptr->next;
        delete ptr;
        ptr = tmp;
        --size;
    }
    null->next = null->prev = null;
    if (size != 0){
        throw "Hrenova";
    }
}

template<class T>
typename Linked2List<T>::Iterator Linked2List<T>::end() const {
    auto result = Iterator(*this);
    result.curr = null;
    return result;
}
