//
// Created by bratjuuc on 16.06.2020.
//
#include <iostream>
#include "../src/List.cpp"

using namespace std;

int main() {

    try {
        Linked2List<int> list = Linked2List<int>();
        list.append(2);

        auto del = new Linked2List<int>(list);
        delete del;

        Linked2List<int> list1(list);
        list.append(4);

        auto it = list1.begin();
        if (it.end()) {
            cout << "false " << __LINE__ << endl;
        }

        Linked2List<int> list2(std::move(list));
        if (list2.isEmpty()) {
            cout << "false " << __LINE__ << endl;
        }


        for (auto i = list2.begin(); !i.end(); ++i) {
            std::cout << i.peek() << " ";
        }
        std::cout << endl<< endl;


        Linked2List<int> list3 = Linked2List<int>();
        list3.append(2);
        list3.append(3);
        list3.append(4);

        //4 3 2
        for (auto i = list3.begin(); !i.end(); ++i) {
            std::cout << i.peek() << " ";
        }
        std::cout << std::endl;

        if (list3.getSize() != 3) {
            cout << "false" << __LINE__ << endl;
        }

        it = list3.first_occurence(2);
        if (2 != *it) {
            cout << "false" << __LINE__ << endl;
        }
        if (it.end()) {
            cout << "false" << __LINE__ << endl;
        }
        else
            cout << "TRUE" << __LINE__ << endl;

        auto it1 = list3.first_occurence(4);
        it1.insert(7);
        it1.insert(21);

        if (21 != *it1) {
            cout << "false" << __LINE__ << endl;
        }
        ++it1;
        if (7 != *it1) {
            cout << "false" << __LINE__ << endl;
        }

        it1.extract();

        if (4 != *it1) {
            cout << "false" << __LINE__ << endl;
        }
        else
            cout << "TRUE" << __LINE__ << endl;

        //21 4 3 2
        for (auto i = list3.begin(); !i.end(); ++i) {
            std::cout << *i << " ";
        }
        cout << "TRUE" << __LINE__ << endl;

        list3.makeEmpty();

        if (!list3.isEmpty()) {
            cout << "false" << __LINE__ << endl;
        }
        cout << "TRUE" << __LINE__ << endl;
        {
            Linked2List<int> list = Linked2List<int>();
            list.append(1).append(2).append(3);
            Linked2List<int> listSum = std::move(list.append(4));
        }
    }
    catch (const char *c) {
        std::cout << c << endl;
    }
    return 0;
}
